﻿using System;
using Learning.Interfaces;
using Learning.Services;

namespace Learning
{
	class Program
	{
		static void Main(string[] args)
		{
			IBankAccount venusAccount = new BankAccount();

			venusAccount.CreateAccount();

			venusAccount.Withdraw(100);
		}
	}
}
