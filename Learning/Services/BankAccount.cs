﻿using System;
using System.Collections.Generic;
using System.Text;
using Learning.EasyWork;
using Learning.Interfaces;

namespace Learning.Services
{
	class BankAccount : IBankAccount
    {

        public decimal _balance=0;

        public void PayIn(decimal amount) { _balance += amount; }

        public bool Withdraw(decimal amount)
        {
            if (_balance >= amount)
            {
                _balance -= amount;

                Console.WriteLine("Withdrawal Successfull.");
                ToString();
                return true;
            }
            Console.WriteLine("Withdrawal attempt failed.");
            return false;
        }
        
        public void CreateAccount()
		{
            string name = HelpInput.Inputs("Enter name: ");
            int age, openAmouunt;

            try
			{
                 age = Convert.ToInt32( HelpInput.Inputs("Enter age: "));
                 openAmouunt = Convert.ToInt32( HelpInput.Inputs("Enter opening balance: "));
                PayIn(openAmouunt);
            }
            catch (Exception e)
			{
                Console.WriteLine("Message: "+e.Message+"\nCause: "+e.Source);
			}

            Console.WriteLine("Account created");

        }
        public decimal Balance => _balance;
        public override string ToString() =>
          $"Access Bank: Balance = {_balance,6:C}";
    }
}
