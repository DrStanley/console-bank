﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Learning.Interfaces
{
	class IBankAccount
	{
		public void PayIn(decimal amount);
		public bool Withdraw(decimal amount);

		public void CreateAccount();

		public decimal Balance { get; }
	}
}
